import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import numpy as np
from math import acos,pi,sin,cos,atan2,pow
import time
import tkinter as tk

#模型初始化参数
base_P=np.eye(4)

L1,L2,L3,L4=5,5,5,5

def A_X(theta):
    return np.mat([[1,0,0,0],
            [0,cos(theta),-sin(theta),0],
            [0,sin(theta),cos(theta),0],
            [0,0,0,1]])
def A_Y(theta):
    return np.mat([[cos(theta),0,sin(theta),0],
            [0,1,0,0],
            [-sin(theta),0,cos(theta),0],
            [0,0,0,1]])
def A_Z(theta):
    return np.mat([[cos(theta),-sin(theta),0,0],
            [sin(theta),cos(theta),0,0],
            [0,0,1,0],
            [0,0,0,1]])
def A_P(x=0,y=0,z=0):
    a1=np.eye(4)
    a1[0][3]=x
    a1[1][3]=y
    a1[2][3]=z
    return np.mat(a1)

def DH_T0(theta,d=L1,a=0,alpha=pi/2):
    return A_Z(theta)*A_P(z=d)*A_P(x=a)*A_X(alpha)
def DH_T1(theta,d=0,a=L2,alpha=0):
    return A_Z(theta)*A_P(z=d)*A_P(x=a)*A_X(alpha)
def DH_T2(theta,d=0,a=L3,alpha=0):
    return A_Z(theta)*A_P(z=d)*A_P(x=a)*A_X(alpha)
def DH_T3(theta,d=0,a=L4,alpha=0):
    return np.mat([[cos(theta),-sin(theta)*cos(alpha),sin(theta)*sin(alpha),a*cos(theta)],
                    [sin(theta),cos(theta)*cos(alpha),-cos(theta)*sin(alpha),a*sin(theta)],
                    [0,        sin(alpha),          cos(alpha),             d            ],
                    [0,        0,         0,                1]],dtype=np.float64)
def DH_T(theta,d=0,a=L2,alpha=pi/2):
    return np.mat([[cos(theta),-sin(theta)*cos(alpha),sin(theta)*sin(alpha),a*cos(theta)],
                    [sin(theta),cos(theta)*cos(alpha),-cos(theta)*sin(alpha),a*sin(theta)],
                    [0,        sin(alpha),          cos(alpha),             d            ],
                    [0,        0,         0,                1]],dtype=np.float64)

class Arm():
    def __init__(self,imitate=False,L1=5,L2=10,L3=10,L4=5) -> None:
        self.fig=plt.figure()
        self.ax3d=self.fig.add_subplot(projection='3d')
        self.window = tk.Tk()
        self.create_form(self.fig)
        self.imitate=imitate
        self.L1=L1
        self.L2=L2
        self.L3=L3
        self.L4=L4
        self.thetas=[0,0,0,0]
        self.postion=[0,0,0,0]
        self.plan_t=0.02
        plt.ion()
        self.window_init()

    def window_init(self):       
        self.window.title('Window of ArmRobot')
        self.window.geometry('1080x800')
        #这里是窗口的内容
        s1=tk.Scale(self.window,label='theta1',from_=0,to=pi,orient=tk.HORIZONTAL,
            length=500,showvalue=1,tickinterval=10,resolution=0.01,command=self.do_selection1)
        s1.pack(anchor='e')
        s2=tk.Scale(self.window,label='theta2',from_=0,to=pi,orient=tk.HORIZONTAL,
            length=500,showvalue=1,tickinterval=10,resolution=0.01,command=self.do_selection2)
        s2.pack(anchor='e')
        s3=tk.Scale(self.window,label='theta3',from_=0,to=pi,orient=tk.HORIZONTAL,
            length=500,showvalue=1,tickinterval=10,resolution=0.01,command=self.do_selection3)
        s3.pack(anchor='e')
        s4=tk.Scale(self.window,label='theta4',from_=0,to=pi,orient=tk.HORIZONTAL,
            length=500,showvalue=1,tickinterval=10,resolution=0.01,command=self.do_selection4)
        s4.pack(anchor='e')

        s5=tk.Scale(self.window,label='x',from_=-10,to=10,orient=tk.HORIZONTAL,
            length=500,showvalue=1,tickinterval=10,resolution=0.01,command=self.do_selection5)
        s5.pack(anchor='e')
        s6=tk.Scale(self.window,label='y',from_=-10,to=10,orient=tk.HORIZONTAL,
            length=500,showvalue=1,tickinterval=10,resolution=0.01,command=self.do_selection6)
        s6.pack(anchor='e')
        s7=tk.Scale(self.window,label='z',from_=0,to=10,orient=tk.HORIZONTAL,
            length=500,showvalue=1,tickinterval=10,resolution=0.01,command=self.do_selection7)
        s7.pack(anchor='e')
        s8=tk.Scale(self.window,label='theta',from_=-pi/2,to=pi/2,orient=tk.HORIZONTAL,
            length=500,showvalue=1,tickinterval=10,resolution=0.01,command=self.do_selection8)
        s8.pack(anchor='e')        
        self.window.mainloop()

    def Arm_imitate(self,coordinates):
        plt.cla()
        self.ax3d.set_xlabel("x")
        self.ax3d.set_ylabel('y')
        self.ax3d.set_zlabel('z')
        self.ax3d.set_xlim3d(left=-20,right=20)
        self.ax3d.set_ylim3d(bottom=-20,top=20)
        self.ax3d.set_zlim3d(bottom=0,top=15)

        self.ax3d.scatter3D(coordinates[:,0],coordinates[:,1],coordinates[:, 2], c='r') # 开始绘制，x[:,0] 表示取第一维
        self.ax3d.plot(coordinates[:,0],coordinates[:,1],coordinates[:, 2],c='b')
        #plt.show()
        #plt.pause(0.02)

    def create_form(self,figure):
        #把绘制的图形显示到tkinter窗口上
        self.canvas=FigureCanvasTkAgg(figure,self.window)
        self.canvas.draw()  #以前的版本使用show()方法，matplotlib 2.2之后不再推荐show（）用draw代替，但是用show不会报错，会显示警告
        self.canvas.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
 
        #把matplotlib绘制图形的导航工具栏显示到tkinter窗口上
        toolbar =NavigationToolbar2Tk(self.canvas, self.window) #matplotlib 2.2版本之后推荐使用NavigationToolbar2Tk，若使用NavigationToolbar2TkAgg会警告
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

    def inverse(self):
        self.thetas[0]=atan2(self.postion[1],self.postion[0])
        T_R=A_X(pi/2)*A_Y(self.thetas[0])*A_Z(self.postion[3])
        T_G=np.array(T_R)
        T_G[0][3]=self.postion[0]
        T_G[1][3]=self.postion[1]
        T_G[2][3]=self.postion[2]
        #以上得到目标矩阵
        T0_i=np.mat([[cos(self.thetas[0]),sin(self.thetas[0]),0,0],
                    [0,0,1,-L1],
                    [sin(self.thetas[0]),-cos(self.thetas[0]),0,0],
                    [0,0,0,1]])
        T=np.array(T0_i*T_G)      #把目标矩阵与T0的逆运算得到T_123,简记为T
        y1=T[0][3]-T[0][0]*L3
        y2=T[1][3]-T[1][0]*L3
        
        self.thetas[2]=-acos((y1**2+y2**2-L1**2-L2**2)/(2*L1*L2))  #可能还要求asin 以确定象限,#get the velua of theta2
        temp=(L2*sin(self.thetas[2]))**2+(L1+L2*cos(self.thetas[2]))**2
        s1=(y2*(L2*cos(self.thetas[2])+L1)-y1*L2*sin(self.thetas[2]))/temp
        c1=(y2*L2*sin(self.thetas[2])+y1*(L2*cos(self.thetas[2])+L1))/temp
        self.thetas[1]=atan2(s1,c1)     #get the velua of theta1
        t123=atan2(T[1][0],T[0][0])
        self.thetas[3]=t123-self.thetas[2]-self.thetas[1]

    def moveArm(self):
        print('--------------------')
        j1=DH_T0(self.thetas[0])*base_P
        j2=DH_T0(self.thetas[0])*DH_T1(self.thetas[1])*base_P
        j3=DH_T0(self.thetas[0])*DH_T1(self.thetas[1])*DH_T2(self.thetas[2])*base_P
        j4=DH_T0(self.thetas[0])*DH_T1(self.thetas[1])*DH_T2(self.thetas[2])*DH_T3(self.thetas[3])*base_P
        joints_P=np.array([j1[:3,3],j2[:3,3],j3[:3,3],j4[:3,3]])
        coordinates=joints_P.reshape((4,3))
        coordinates=np.vstack(([0,0,0],coordinates))
        print(coordinates)
        if self.imitate==True:
            self.Arm_imitate(coordinates)

    def trajectoryPlan(self,t_array,q_array,v_array,a_array):
        '''
        for example:
        q_array=[0,150]#指定起止位置
        t_array=[0,2]#指定起止时间
        v_array=[0,0]#指定起止速度
        a_array=[0,0]#指定起止加速度
        it also can be :
        q_array=[0,90,150]#指定起止位置
        t_array=[0,1,2]#指定起止时间
        v_array=[0,1,0]#指定起止速度
        a_array=[0,2,0]#指定起止加速度
        '''
        t=[t_array[0]]
        q=[q_array[0]]
        v=[v_array[0]]
        a=[a_array[0]]
        for i in range(len(q_array)-1):
            T = t_array[i+1] - t_array[i]  # 时间差
            a0 = q_array[i]
            a1 = v_array[i]
            a2 = a_array[i] / 2
            a3 = (20 * q_array[i + 1] - 20 * q_array[i] - (8 * v_array[i+ 1] + 12 * v_array[i]) * T - (3 * a_array[i] - a_array[i + 1]) * pow(T, 2)) / (2 * pow(T, 3))
            a4 = (30 * q_array[i] - 30 * q_array[i+ 1] + (14 * v_array[i+ 1] + 16 * v_array[i]) * T + (3 * a_array[i] - 2 * a_array[i+ 1]) * pow(T, 2)) / (2 * pow(T, 4))
            a5 = (12 * q_array[i+ 1] - 12 * q_array[i] - (6 * v_array[i+ 1] + 6 * v_array[i]) * T - ( a_array[i] - a_array[i+ 1]) *pow(T, 2)) / (2 * pow(T, 5))
            ti = np.arange(t_array[i], t_array[i+ 1],self.plan_t)
            '''
            求解出角度，角速度，角加速度随某个时间区间随时间变换的曲线
            '''
            qi = a0 + a1 * (ti - t_array[i]) + a2 * np.power((ti - t_array[i]), 2) + a3 * np.power((ti - t_array[i]), 3) + a4 * np.power( (ti - t_array[i]), 4) + a5 * np.power((ti - t_array[i]), 5)
            vi = a1 + 2 * a2 * (ti - t_array[i]) + 3 * a3 * np.power((ti - t_array[i]), 2) + 4 * a4 * np.power( (ti - t_array[i]), 3) + 5 * a5 * np.power((ti - t_array[i]), 4)
            ai = 2 * a2 + 6 * a3 * (ti - t_array[i]) + 12 * a4 * np.power((ti - t_array[i]), 2) + 20 * a5 * np.power((ti - t_array[i]), 3)
            ti = ti.tolist()  # 转换为list，否则进行数据整合会报错
            qi = qi.tolist()  # 转换为list，否则进行数据整合会报错
            vi = vi.tolist()  # 转换为list，否则进行数据整合会报错
            ai = ai.tolist()  # 转换为List，否则进行数据整合会报错
            t = t + ti[1:] #进行数据整合，用来绘制函数图像
            q = q + qi[1:] #进行数据整合，用来绘制函数图像
            v = v + vi[1:] #进行数据整合，用来绘制函数图像
            a = a + ai[1:] #进行数据整合，用来绘制函数图像
        plt.plot(t, q, linestyle=":", color=(1, 0, 0, 1.0), label="angle curve")
        plt.scatter(t_array, q_array, marker='*', s=70, label="angle spot", zorder=3)

        plt.plot(t,v, linestyle="-", color=(1, 0.5, 1, 1.0), label="speed curve")
        plt.scatter(t_array, v_array, marker='^', s=70, label="speed spot", zorder=3)

        plt.plot(t, a, linestyle="-.", color=(0, 0, 0, 1.0), label="acceleration curve")
        plt.scatter(t_array, a_array, marker='o', s=70, label="acceleration spot", zorder=3)

        plt.grid()#显示网格
        plt.legend()#显示图例
        plt.show()
        plt.pause(1)
        plt.cla()
        return q

    def GoPlan(self,t,x,y,z,theta):
        x_plan=self.trajectoryPlan(t,x[0],x[1],x[2])
        y_plan=self.trajectoryPlan(t,y[0],y[1],y[2])
        z_plan=self.trajectoryPlan(t,z[0],z[1],z[2])
        theta_plan=self.trajectoryPlan(t,theta[0],theta[1],theta[2])
        point_num=int((t[-1]-t[0])/self.plan_t)-1
        for i in range(point_num):
            self.postion=[x_plan[i],y_plan[i],z_plan[i],theta_plan[i]]
            self.inverse()
            self.moveArm()
            time.sleep(self.plan_t)
        

    def endEffector(self,j5):
        duty5=j5*(1/18)+2.5
        time.sleep(0.5)
        return j5

    def do_selection1(self,v):
        self.thetas[0]=float(v)
        self.moveArm()
    def do_selection2(self,v):
        self.thetas[1]=float(v)
        self.moveArm()
    def do_selection3(self,v):
        self.thetas[2]=float(v)
        self.moveArm()
    def do_selection4(self,v):
        self.thetas[3]=float(v)
        self.moveArm()
    
    def do_selection5(self,v):
        self.postion[0]=float(v)
        self.inverse()
        self.moveArm()
    def do_selection6(self,v):
        self.postion[1]=float(v)
        self.inverse()
        self.moveArm()
    def do_selection7(self,v):
        self.postion[2]=float(v)
        self.inverse()
        self.moveArm()
    def do_selection8(self,v):
        self.postion[3]=float(v)
        self.inverse()
        self.moveArm()

if __name__=='__main__':
    myArm=Arm(imitate=True)